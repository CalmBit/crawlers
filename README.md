# crawlers
***
_You wake up in a dark cave, not knowing where you are or who you are.
Next to the wall, lies a dim torch hung on a steel rack, a rusty sword,
a pouch, and a small note attached to the sword.
Now, you begin._
***
### Controls:
WASD Standard

Space - Use Item

G - Use Selected Spell

J - Enchant current item (must be tool)

F8 - Spawn Zombie

O - Down 1 Inventory Slot

P - Up 1 Inventory Slot

; - Down 1 Material Slot

' - Up One Material Slot

. - Down One Spell

/ - Up One Spell

N - DEBUG (USE ONLY IF TESTING MODIFICATIONS-VERY ANNOYING)

ESC - EXIT DEBUG

Click on entities (including yourself) to kill them.

F - Save (broken)

On The Menu:

Top Button = Quit

Lower Button = Play
***
###Notes From Ethan:
Hello Reader!
Well, we've done it! After six total months of tireless work,
the Titan engine and Crawler-Tombs Of Darkness is offically in
Alpha! From here, the sky is the limit as to features we want to
implement in order to improve the game. We have gotten this far,
and we'll get farther.
-Ethan Brooks
***
###Credits:
Ethan Brooks - CEO, Head Of Code Work
Shudder Hurd-Burnell - Co-CEO, Head Of Artistic Design
###Other Notes:
Visit Our Website at http://agelasticstudios.jumpingcrab.com !
