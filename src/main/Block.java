package main;

import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import static org.lwjgl.opengl.GL11.*;

public class Block {
	//Store position
	public Vector2f pos;
	//Store old position when a change is invoked
	public Vector2f oldPos;
	public float R,G,B;
	public Game game;
	public BoundingBox bounds;
	public Texture texture;
	public Block(Material material,int index,int ID,Game game) {
		pos = new Vector2f();
		oldPos = new Vector2f();
		R = .3f;
		G = .3f;
		B = .3f;
		this.game = game;
		bounds = new BoundingBox(pos.x, pos.y, 64, 64, game);
	}
	public void changePos(Vector2f newPos) {
		oldPos = pos;
		pos = newPos;
	}
	public void render() {
		Color.gray.bind();
		texture.bind();
		//glColor3f(R,G,B);
		glBegin(GL_QUADS);
		glTexCoord2f(0,0);
		glVertex2f(pos.x, pos.y);
		glTexCoord2f(1,0);
		glVertex2f(pos.x + 64, pos.y);
		glTexCoord2f(1,1);
		glVertex2f(pos.x + 64, pos.y + 64);
		glTexCoord2f(0,1);
		glVertex2f(pos.x, pos.y + 64);
		glEnd();
	}
	public void tick() {
		render();
		bounds.updateBounds(pos.x, pos.y, 64, 64);
	}
	public void updateNeighbors() {
		
	}
	public void onCollide(Entity ent) {
		
	}
}
