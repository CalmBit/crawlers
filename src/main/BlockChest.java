package main;

public class BlockChest extends Block {

	public ItemStack content[];
	public int selector;
	public EntityPlayer interactor = null;
	
	public BlockChest(Material material, int index, int ID, Game game) {
		super(material, index, ID, game);
		content = new ItemStack[32];
		selector = 0;
		texture = Game.chestTexture;
	}

	public void selectContainer(EntityPlayer ent)
	{
		if(interactor == null)
		{
			interactor = ent;
				if(content[selector] != null) {
					if(content[selector].stackQ > 1) 
						System.out.println("[INFO] Hooked into chest-Currently Selected: " + content[selector].stackQ + " of " + content[selector].item.name + ".");
					else
						System.out.println("[INFO] Hooked into chest-Currently Selected: A(n) " + content[selector].item.name);
				}
				else System.out.println("[INFO] Hooked into chest-Current Selection Is Empty.");
			interactor.useTimer = 500;
			}
		else
		{
			System.out.println("[INFO] " + interactor.username + " is already using this chest!");
		}
	}
	
	public void deselectContainer()
	{
		if(interactor != null)
		{
			interactor = null;
			System.out.println("[INFO] Deselected chest.");
		}
	}
	
	public void insertItem(int slot, ItemStack toPut) {
	if(toPut != null) {
		if(content[slot] == null)
		{
			content[slot] = toPut;
			interactor.inventory.pInv[interactor.inventory.selection] = null;
			System.out.println("[INFO] Inserted " + content[slot].stackQ + " of "  + content[slot].item.name);
		}
		else if(content[slot].item == toPut.item && (content[slot].stackQ + toPut.stackQ) <= content[slot].item.maxStack)
		{
			content[slot].stackQ += toPut.stackQ;
			System.out.println("[INFO] Inserted " + toPut.stackQ + " more of " + content[slot].item.name);
			interactor.inventory.pInv[interactor.inventory.selection] = null;
			
		}
		else
		{
			System.out.println("[INFO] Slot isnt empty! It still contains " + content[slot].name);
		}
		
	}
	else
	{
		System.out.println("[INFO] Nothing to put into chest!");
	}
	}
	
	public void createItem(int slot, ItemStack toPut)
	{
		content[slot] = toPut;
	}

	public void removeItem(int slot) {
		if(content[slot] != null && interactor.inventory.pInv[interactor.inventory.selection] == null)
		{
			interactor.inventory.pInv[interactor.inventory.selection] = new ItemStack(content[slot].item,1);
			System.out.println("[INFO] Added a(n) " + content[selector].item.name + " to the slot.");
			if(content[slot].stackQ == 1)
			{
				content[slot] = null;
			}
			else {
				content[slot].stackQ--;
			}
			
		}
		else if(content[slot] != null && interactor.inventory.pInv[interactor.inventory.selection].item == content[slot].item)
		{
			interactor.inventory.pInv[interactor.inventory.selection].stackQ++;
			System.out.println("[INFO] Added another " + content[selector].item.name + " to the slot.");
			if(content[slot].stackQ == 1)
			{
				content[slot] = null;
			}
			else {
				content[slot].stackQ--;
			}
		}
		else if(content[slot] != null && interactor.inventory.pInv[interactor.inventory.selection].item != null)
		{
			System.out.println("[INFO] There is already an item within this inventory space!");
		}
		else if(content[slot] == null)
		{
			System.out.println("[INFO] Slot is empty!");
		}
		else {
			System.out.println("[ERROR] Unknown confliction from BlockChest at {" + pos.getX() + "," + pos.getY() + "}.");
		}
	}

	public ItemStack getSelection() {
		return content[selector];
		
	}


		
}



