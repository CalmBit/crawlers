package main;

import static org.lwjgl.opengl.GL11.*;
public class BlockNote extends Block {

	String line1;
	String line2;
	String line3;
	String line4;
	String line5;
	String line6;
	String sig;
	
	public EntityPlayer reader;
	public boolean isReading;
	public BlockNote(Material material, int index, int ID, Game game, String line1, String line2, String line3, String line4, String line5, String line6, String sig) {
		super(material, index, ID, game);
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
		this.line4 = line4;
		this.line5 = line5;
		this.line6 = line6;
		this.sig = sig;
		texture = Game.noteTexture;
	}
	
	public void onRead(EntityPlayer reader){
		this.reader = reader;
		isReading = true;
	}
	
	public void stopReading()
	{
		isReading = false;
	}
	public void tick()
	{
		super.tick();
		if(isReading)
		{
			glColor3f(1f,1f,1f);
			glBegin(GL_QUADS);
			glVertex2f(0,0);
			glVertex2f(game.screenX,0);
			glVertex2f(game.screenX,game.screenY);
			glVertex2f(0,game.screenY);
			glEnd();
		
		
			game.notefont.drawString(0, game.screenY/8, line1);
			
			game.notefont.drawString(0, game.screenY/8 + 50, line2);
				
			game.notefont.drawString(0, game.screenY/8 + 100, line3);
			
			game.notefont.drawString(0, game.screenY/8 + 150, line4);
			
			game.notefont.drawString(0, game.screenY/8 + 200, line5);
			
			game.notefont.drawString(0, game.screenY/8 + 250, line6);
			
			game.notefont.drawString(game.screenX - 400, game.screenY/8 + 500, sig);
				
			}
		}
			

		
	}
	
