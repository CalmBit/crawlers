package main;

import org.lwjgl.util.vector.Vector2f;

public class BoundingBox {

	public Vector2f min,max;
	public Game game;
	public BoundingBox(float minX, float minY, float maxX, float maxY, Game game) {
		this.min = new Vector2f(minX,minY);
		this.max = new Vector2f(maxX,maxY);
		this.game = game;
	}
	
	public void updateBounds(float newMinX, float newMinY, float newMaxX, float newMaxY) {
		this.min = new Vector2f(newMinX,newMinY);
		this.max = new Vector2f(newMaxX,newMaxY);
	}
	
	public int doesIntersect(BoundingBox collider, BoundingBox collidee) {
		
		if(collider.min.x <= collidee.min.x + 64 && collider.min.x + 64 >= collidee.min.x + 64 && collider.min.y <= collidee.min.y + 64 && collider.min.y + 64 >= collidee.min.y) {
			return 1;
		}
		if(collider.min.x + 64 >= collidee.min.x && collider.min.x + 64 <= collidee.min.x + 64 && collider.min.y <= collidee.min.y + 64 && collider.min.y + 64 >= collidee.min.y) {
			return 2;
		}
		if(collider.min.y <= collidee.min.y  + 64 && collider.min.y + 64 >= collidee.min.y + 64 && collider.min.x <= collidee.min.x + 64 && collider.min.x + 64 >= collidee.min.x) {
			return 3;
		}
		if(collider.min.y + 64 >= collidee.min.y && collider.min.y + 64 <= collidee.min.y  + 64 && collider.min.x <= collidee.min.x + 64 && collider.min.x + 64 >= collidee.min.x) {
			return 4;
		}
		return 0;
	}
	public Vector2f getMin() {
		return min;
	}
	
	public Vector2f getMax() {
		return max;
	}
}
