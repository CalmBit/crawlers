package main;

public class CraftingCombination {

	public IngredientBase in1;
	public IngredientBase in2;
	public IngredientBase in3;
	public ItemDynamic result;
	
	/*public static CraftingCombination greenGunpowder = new CraftingCombination(IngredientDeclerationContainer.gunpowder,IngredientDeclerationContainer.greendust,(ItemDynamic) Item.greenGunpowder);*/
	public CraftingCombination(IngredientBase in1,IngredientBase in2,IngredientBase in3, ItemDynamic result)
	{
		this.in1 = in1;
		this.in2 = in2;
		this.in3 = in3;
		this.result = result;
	}
	public CraftingCombination(IngredientBase in1,IngredientBase in2, ItemDynamic result)
	{
		this.in1 = in1;
		this.in2 = in2;
		this.in3 = null;
		this.result = result;
	}
}
