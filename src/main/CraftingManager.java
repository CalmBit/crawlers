package main;

public class CraftingManager {

	
	public ItemDynamic craftItem(CraftingCombination combination, IngredientStack i1, IngredientStack i2, IngredientStack i3, int q1, int q2, int q3)
	{
		if(i1.in != combination.in1 ||i1.in != combination.in2 || i1.in != combination.in3 )
		{
			System.out.println("[ERROR] First Ingredient Not Part Of Selected Recipie!");
		}
		else if(i2.in != combination.in1 ||i2.in != combination.in2 || i2.in != combination.in3 )
		{
			System.out.println("[ERROR] Second Ingredient Not Part Of Selected Recipie!");
		}
		else if(i3.in != combination.in1 ||i3.in != combination.in2 || i3.in != combination.in3 )
		{
			System.out.println("[ERROR] Third Ingredient Not Part Of Selected Recipie!");
		}
		else {
			ItemDynamic result = combination.result;
					result.q1 = i1.amount;
					result.q2 = i2.amount;
					result.q3 = i3.amount;
					result.amount = (int) ((int) (result.q1 * i1.in.ratio) + (result.q2 * i2.in.ratio) + (result.q3 * i3.in.ratio) * result.ratio);
					return result;
		}
		return null;
		
	}
}
