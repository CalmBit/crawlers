package main;

public class EnchantmentBase {

	public byte level;
	public Item appliedTool;
	
	//1 is damage,2 is mining, 3 up is undefined
	public byte effect;
	
	
	public EnchantmentBase(Item toolToAdd,byte level)
	{
		appliedTool = toolToAdd;
		this.level = level;
	}
}
