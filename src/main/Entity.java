package main;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Random;

import org.json.simple.JSONValue;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import static org.lwjgl.opengl.GL11.*;

//import org.newdawn.slick.

public class Entity {
	public Vector3f pos;
	public BoundingBox bounds;
	public int maxHealth;
	public int health;
	public int fireTimer;
	public int breathTimer;
	public boolean isDead;
	public int baseDamage;
	//check if touching walls
	public boolean touchingNorthWall;
	public boolean touchingSouthWall;
	public boolean touchingEastWall;
	public boolean touchingWestWall;
	public Random posrand = new Random();
	public Random entRandom = new Random();
	public Texture texture;
	public float a;
	public String name;
	public float r,g,b;
	public Game game;
	public int sizeX, sizeY = 16;
	int x = Mouse.getX(); // will return the X coordinate on the Display.
	int y = Mouse.getY(); // will return the Y coordinate on the Display.
	public boolean load;

public Entity(String name, Game game, Texture texture) {
	this.game = game;
	maxHealth = 0;
	health = 0;
	fireTimer = 6000;
	breathTimer = 12000;
	isDead = false;
	baseDamage = 0;
	a = posrand.nextFloat();
	r = 1f;
	g = 0f;
	b = 0f;
	this.texture = texture;
	
	pos = new Vector3f(a*800,a*600,0);
	this.name = name;
	if(game.isDebugMode)
	{
	System.out.println("[DEBUG] Entity " + name + " loaded.");
	}
	bounds = new BoundingBox(pos.x,pos.y,64f,64f,game);
	touchingNorthWall = false;
	touchingSouthWall = false;
	touchingWestWall = false;
	touchingEastWall = false;
	load = true;
}

/*public boolean getIsDead() {
	return isDead;
}*/

	public void onCollision() {
	
	}


public void onAttacked(Entity attacker,int Damage) {
	
}
public void tick() {
	if(Mouse.getX() >= pos.x && Mouse.getX() <= (pos.x+64) && Mouse.getY() >= pos.y && Mouse.getY() <= (pos.y+64) && Mouse.isButtonDown(0)) health -= .1;
	//if(pos.y <= 0) pos.y = 0;
	//if(pos.y + 64 >= 600) pos.y = 564;
	//if(pos.x <= 0) pos.x = 0;
	//if(pos.x + 64 >= 800) pos.x = 764;
	bounds.updateBounds(pos.x,pos.y,64,64);
	if(bounds.min.x <= 0) {
		pos.x = 0;
		touchingWestWall = true;
	}
	else touchingEastWall = false;
	if(bounds.min.x + 64 >= game.screenX){
		pos.x = game.screenX - 64; 
		touchingEastWall = true;
	}
	else touchingEastWall = false;
	if(bounds.min.y <= 0){
		pos.y = 0;
		touchingSouthWall = true;
	}
	else touchingSouthWall = false;
	if(bounds.min.y + 64 >= game.screenY){
		pos.y = game.screenY - 64;
		touchingNorthWall = true;
	}
	else touchingNorthWall = false;
}

public void cleanup() {
	health = 0;
	pos = null;
	bounds = null;
}


public void render(float r, float g, float b) {
		
	if(texture == null)
	{
		System.out.println("[ERROR] Texture returned null in Entity " + name + "; Reverting to player texture.");
		texture = Game.playerTexture;
	}
		Color.white.bind();
		glBindTexture(GL_TEXTURE_2D,this.texture.getTextureID());
		//GL11.glColor3f(r,g,b);
		glBegin(GL_QUADS);
		glTexCoord2f(0,0);
		glVertex2f(pos.x,pos.y);
		glTexCoord2f(1,0);
		glVertex2f(pos.x+64,pos.y);
		glTexCoord2f(1,1);
		glVertex2f(pos.x+64,pos.y+64);
		glTexCoord2f(0,1);
		glVertex2f(pos.x,pos.y+64);
		glEnd();
		
		//game.playerTexture.release();
		
	
	
	if(game.isDebugMode) {
	glBegin(GL_LINES);
	glColor3f(0f,0f,0f);
	glVertex2f(bounds.min.x,bounds.min.y);
	glVertex2f(bounds.min.x+bounds.max.x,bounds.min.y);
	
	glVertex2f(bounds.min.x+bounds.max.x,bounds.min.y);
	glVertex2f(bounds.min.x+bounds.max.x,bounds.min.y+bounds.max.y);
	
	glVertex2f(bounds.min.x+bounds.max.x,bounds.min.y+bounds.max.y);
	glVertex2f(bounds.min.x,bounds.min.y+bounds.max.y);
	
	glVertex2f(bounds.min.x,bounds.min.y+bounds.max.y);
	glVertex2f(bounds.min.x,bounds.min.y);
	glEnd();
	}
	
}





}
