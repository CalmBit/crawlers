package main;

public abstract class EntityAI {

	private int groupInt;
	
	public EntityAI() {
		groupInt = 0;
	}
	
	public abstract boolean mayExecute();
	
	public boolean continued() {
		return mayExecute();
	}
	
	public boolean doesContinue() {
		return true;
	}
	
	public void update() {
		
	}
	
	public void start() {
		
	}
	public void reset() {
		
	}
	
	public void setGroup(int int1) {
		groupInt = int1;
	}
	
	public int getGroup() {
		return groupInt;
	}
}
