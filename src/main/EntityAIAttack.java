package main;

public class EntityAIAttack extends EntityAI {

	public int damage;
	public EntityLiving attacker;
	
	
	
	public EntityAIAttack(EntityZombie attacker2, int damage) {
		this.attacker = attacker2;
		this.damage = damage;
	}

	public void update()
	{
		if(attacker.demeanor == EnumDemeanor.AGRESSIVE && attacker.hasSighted && attacker.targetedEntity instanceof EntityLiving)
		{
			attacker.attackEntity(attacker.targetedEntity,attacker,attacker.damage,attacker.targetedEntity.pos,attacker.pos);
		}
		
		
	}
	public boolean mayExecute() {
		return false;
	}

}
