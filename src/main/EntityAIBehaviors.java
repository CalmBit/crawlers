package main;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class EntityAIBehaviors  {

	public Set<EntityAI> behaviorsWaiting = new HashSet<EntityAI>();
	public Set<EntityAI> behaviorsExecuting = new HashSet<EntityAI>();;
	
	public EntityAIBehaviors() {
		
	}
	
	public void addBehavior(int priority,EntityAI behavior) {
		if(!behaviorsWaiting.contains(behavior))
		behaviorsWaiting.add(behavior);
		else System.out.println("[ERROR] Behavior " + behavior.toString() + " already exists in this entity!");
	}
	public void updateExecutingBehaviors()
	{
		for(EntityAI beha : behaviorsExecuting)
		{
			beha.update();
		}
	}
	public void executeBehavior(EntityAI behavior){
		if(behaviorsWaiting.contains(behavior)) {
			behaviorsExecuting.add(behavior);
			behaviorsWaiting.remove(behavior);
		}
	}
}
