package main;

import java.io.PrintStream;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public class EntityAISight extends EntityAI
{
  EntityLiving entity;
  public float r = 1;
  public float g = 1;
  public float b = 1;

  public EntityAISight(EntityLiving ent) {
    this.entity = ent;
  }

  public void update() {
    super.update();
    Game game = this.entity.game;

    if(game.isDebugMode) {
    GL11.glColor3f(this.r, this.g, this.b);
    GL11.glBegin(GL11.GL_QUADS);
    GL11.glVertex2f(this.entity.pos.x - 50.0F, this.entity.pos.y - 50.0F);
    GL11.glVertex2f(this.entity.pos.x + 100.0F, this.entity.pos.y - 50.0F);
    GL11.glVertex2f(this.entity.pos.x + 100.0F, this.entity.pos.y + 100.0F);
    GL11.glVertex2f(this.entity.pos.x - 50.0F, this.entity.pos.y + 100.0F);
    GL11.glEnd();
    }
    for (EntityLiving potTarg : game.entities)
    {
    if(potTarg != entity && potTarg.demeanor == EnumDemeanor.NEUTRAL || potTarg.demeanor == EnumDemeanor.PASSIVE) {
      if ((potTarg.x + 50 <= this.entity.pos.x + 100.0F) && (potTarg.x >= this.entity.pos.x - 50.0F) && (potTarg.y + 50 <= this.entity.pos.y + 100.0F) && (potTarg.y >= potTarg.y))
      {
        System.out.println(potTarg.name);
      }
    }
    }
  }

  public boolean mayExecute() {
    return false;
  }
}