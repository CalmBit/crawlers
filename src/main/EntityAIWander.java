package main;

import java.util.Random;




import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.vector.Vector2f;

public class EntityAIWander extends EntityAI {
	
	public Random a = new Random();
	public float b;
	public float speed;
	public Entity ent;
	public boolean hitx,hity;
	public Vector2f pathFinish = new Vector2f();
	public Vector2f pathStart = new Vector2f();
	public Vector2f stopPoint = new Vector2f();
	public int stopTimer;
	public EntityAIWander(float speed, Entity ent) {
		this.speed = speed;
		this.ent = ent;
		pathStart.x = this.ent.pos.x;
		pathStart.y = this.ent.pos.y;
		pathFinish.x = a.nextFloat() * ent.game.screenX;
		pathFinish.y = a.nextFloat() * ent.game.screenY;
		stopTimer = a.nextInt(600) + 240;
		showPathFinish();
		
		
	}
	public boolean mayExecute() {
		return false;
	}
	
	public void update(){
		super.update();
		showPathFinish();
		if(!(hitx && hity)) {
		if(ent.pos.x + 25 >= pathFinish.x-10 && ent.pos.x + 25 <= pathFinish.x+10) {
			hitx = true;
			stopPoint(ent.pos.x,stopPoint.x);
		}
		else {
		if(pathFinish.x > pathStart.x) {
			ent.pos.x += speed;
		}
		else if(pathFinish.x < pathStart.x) {
			ent.pos.x -= speed;
		}
	}
		if(ent.pos.y + 25 >= pathFinish.y-10 && ent.pos.y + 25 <= pathFinish.y+10) {
			hity = true;
			stopPoint(ent.pos.y,stopPoint.y);
		}
		else {
				if(pathFinish.y > pathStart.y) {
					ent.pos.y += speed;
				}
				else if(pathFinish.y < pathStart.y) {
					ent.pos.y -= speed;
				}
		}
		}
		else if(stopTimer > 0){
			stopTimer -= 1;
		}
		else {
			getNewPoint();
			if(Game.isDebugMode)
			{
			System.out.println("[DEBUG] " + ent.name + " has chosen a new wander point. Total New Stand Time: " + stopTimer);
			}
		}
		
}
	
	public void stopPoint(float stop,float point) {
		point = stop;
		stop  = stop;
	}
	
	public void showPathFinish() {
		if(Game.isDebugMode) {
		glColor3f(1f, 0f, 1f);
		glBegin(GL_QUADS);
		glVertex2f(pathFinish.x + 10, pathFinish.y + 10);
		glVertex2f(pathFinish.x - 10,pathFinish.y + 10 );
		glVertex2f(pathFinish.x - 10,pathFinish.y - 10 );
		glVertex2f(pathFinish.x + 10,pathFinish.y - 10 );
		glEnd();
		}
	}
	
	public void getNewPoint() {
		hitx = false;
		hity = false;
		pathStart.x = this.ent.pos.x;
		pathStart.y = this.ent.pos.y;
		pathFinish.x = a.nextFloat() * ent.game.screenX;
		pathFinish.y = a.nextFloat() * ent.game.screenY;
		stopTimer = a.nextInt(600) + 240;
	}

}
