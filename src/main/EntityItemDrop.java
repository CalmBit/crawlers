package main;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

public class EntityItemDrop extends Entity {

	public Item item;
	public int index;
	public int meta;

	public EntityItemDrop(Item item, Game game, Texture texture) {
		super(item.name, game, texture);
		this.item = item;
		meta = item.metadeta;
		index = item.ID;
	}

	
	public void tick() {
		super.tick();
	}
	
/*	public void onPickup(EntityLogical ent) {
		if(meta == 0) {
			
		}
		else if(meta == 1) {
			if(ent.health < ent.maxHealth){
				if(ent.health + 50 > ent.maxHealth) {
					ent.health = ent.maxHealth;
				}
				else ent.health += 50;
			}
			
		}
		else if(meta == 2) {
			if(ent.mana < ent.maxMana) {
			if(ent.mana + 50 > ent.maxMana) {
				ent.mana = ent.maxMana;
			}
			else ent.mana += 50;
			}
		}
		else;
	}*/
	public void render(float r, float g, float b){
		
		super.render(r, g, b);
	}

}
