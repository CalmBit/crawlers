package main;

import static org.lwjgl.opengl.GL11.*;

import java.util.Random;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;

public class EntityLiving extends Entity {

	public int age;
	protected int ageCounter;
	public EnumDemeanor demeanor;
	public EntityLiving targetedEntity;
	public boolean hasSighted;
	public int damage;
	public boolean hasAttacked;
	public int attackTimer;
	public int lastAttacker;
	public Random random = new Random();
	public int uID;
	public boolean running;
	public boolean wasDamaged;
	public int dTimer;
	public float or,og,ob;
	public boolean inRange;
	public int aTimer;
	protected EntityAIBehaviors behaviors = new EntityAIBehaviors();
	public ShieldBase shield;
	public EntityLiving(String name, Game game,EnumDemeanor demeanor, Texture texture) {
		super(name, game, texture);
		game.addEntity(this);
		maxHealth = 25;
		health = 25;
		age = 0;
		ageCounter = 0;
		this.demeanor = demeanor;
		hasAttacked=false;
		attackTimer=0;
		uID = random.nextInt(100000); 
		
	}
	
	public void render() {
		super.render(1f, 0f, 0f);
		
		//ugly class hack to check for player
		
			glBegin(GL_QUADS);
			glColor3f(1f,0f,0f);
			glVertex2f(pos.x,pos.y + 75);
			glVertex2f(pos.x + maxHealth, pos.y + 75);
			glVertex2f(pos.x + maxHealth, pos.y + 90);
			glVertex2f(pos.x , pos.y + 90);
			
			glColor3f(0f,1f,0f);
			glVertex2f(pos.x,pos.y + 75);
			glVertex2f(pos.x + health, pos.y + 75);
			glVertex2f(pos.x + health, pos.y + 90);
			glVertex2f(pos.x , pos.y + 90);
			glEnd();
		
	}
	

	public void attackEntity(EntityLiving target, EntityLiving source, int damage, Vector3f attackerPos, Vector3f defenderPos)
	{
		if(!source.hasAttacked && !target.isDead && target != source) {
			//check for player, if so,check if char is holding a sword
			if(source instanceof EntityPlayer)
			{
				EntityPlayer entity = (EntityPlayer)source;
				if(entity.inventory.pInv[entity.inventory.selection] != null)
				{
					damage += entity.inventory.pInv[entity.inventory.selection].item.damage;
					System.out.println(entity.inventory.pInv[entity.inventory.selection].item.damage + " was added to damage.");
					if(entity.inventory.pInv[entity.inventory.selection].item instanceof ItemTool)
					{
						ItemTool tool = (ItemTool) entity.inventory.pInv[entity.inventory.selection].item;
						for(EnchantmentBase enchantment : tool.enchantmentList)
						{
							if(enchantment.effect == 1)
								damage += enchantment.level;
							}
						}
					}
				}
					
					
					
					
					target.health -= damage;
				}
			
			target.health -= damage;
			source.hasAttacked = true;
			aTimer = 50;
			target.wasDamaged = true;
			if(Game.isDebugMode) {
			System.out.println("[DEBUG] Damage Was Caused by " + source.name + " to " + target.name + " which caused "  + damage);
			}
			//target.lastAttacker = source.uID;
			
			if(target instanceof EntityMonster)
			{
				target.demeanor = EnumDemeanor.AGRESSIVE;
			}
			if(target instanceof EntityMob)
			{
				target.running = true;
			}
		}
		
	
	public void pursueEntity(EntityLiving pursuer, EntityLiving target)
	{
		boolean trigger = false;
		if(target.pos.y - 10 >= pursuer.pos.y + 50)
		{
			pursuer.pos.y += .5;
			trigger = true;
		}
		if(target.pos.y + 60 <= pursuer.pos.y)
		{
			pursuer.pos.y -= .5;
			trigger = true;
		}
		if(target.pos.x - 10 >= pursuer.pos.x + 50)
		{
			pursuer.pos.x += .5;
			trigger = true;
		}
		if(target.pos.x + 60 <= pursuer.pos.x)
		{
			pursuer.pos.x -= .5;
			trigger = true;
		}
		if(!trigger){
			inRange = true;
		}
		else {
			inRange = false;
		}
	}
	public void onAttacked()
	{
		
	}
	public void tryExecute(EntityAI behavior) {
		behaviors.executeBehavior(behavior);
	}
	
	public void cleanup(){
		super.cleanup();
		behaviors.behaviorsWaiting.clear();
		behaviors.behaviorsExecuting.clear();
	}
	
	public void tick() {
		super.tick();
		ageCounter++;
		if(health <= 0)
		{
			isDead = true;
		}
		if(hasAttacked)
		{
			attackTimer++;
			//System.out.println("Attack Reset");
			if(attackTimer > 50)
			{
				hasAttacked = false;
				attackTimer = 0;
			}
		}
		
		if(shield != null)
		{
			shield.tick();
			shield.render();
		}
		//Weird Hack to execute behaviors
		for(EntityAI behavior : behaviors.behaviorsWaiting) {
			//System.out.println("Behavior " + behavior.toString() + " executed!");
			tryExecute(behavior);
		}
		for(EntityAI behavior : behaviors.behaviorsExecuting) {
			//System.out.println("Behavior " + behavior.toString() + " updated!");
			behavior.update();
		
		//Planned deprication on account of bad design
			
		/*if(wasDamaged)
		{
		 int d = random.nextInt(4);
		 switch(d)
		 {
		 case 1:
			 pos.x += random.nextFloat();
			 break;
		 case 2:
			 pos.x -= random.nextFloat();
			 break;
		 case 3:
			 if(touchingSouthWall == false) {
			 pos.y += random.nextFloat();
			 }
			 else d = 4;
			 break;
		 case 4:
			 if(touchingNorthWall == false)
			 {
			 pos.y -= random.nextFloat();
			 }
			 else d = 3;
			 break;
			 
		 default:
			 break;
		 }
		 dTimer++;
		 if(dTimer >= 200)
		 {
			 wasDamaged = false;
			 dTimer = 0;
			 r = or;
			 g = og;
			 b = ob;
		 }
		}
		*/
		
		if(ageCounter == 500) {
			age += 1;
			//System.out.println(name + " has aged to " + age + ".");
			ageCounter = 0;
		}
		//if(age == 20) health = 0;

	}
	if(this instanceof EntityZombie)
	{
		pursueEntity(this,game.player);
		if(inRange)
		{
			attackEntity(game.player, this, damage, pos, game.player.pos);
		}
	}

}
}