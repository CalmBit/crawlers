package main;


import static org.lwjgl.input.Keyboard.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.opengl.Texture;


public class EntityLogical extends EntityLiving {

	public int maxMana;
	public int mana;
	public int force;
	public boolean touchingItemY;
	public boolean touchingItemX;
	public SpellBook spells = new SpellBook(this,game);
	public SpellBase selectedSpell;
	public Map<Integer, SpellBase> spellsLearned = new HashMap<Integer, SpellBase>();
	public int currentSpellNum;
	public boolean blockedM;
	int test = 0;
	int count = 0;
	public boolean hookedIntoChest;
	public BlockChest hookedChest;
	
	public EntityLogical(String name, Game game,EnumDemeanor demeanor, Texture texture) {
		super(name, game,demeanor, texture);
		maxMana = 128;
		mana = 128;
		force = 0;
		touchingItemX = false;
		touchingItemY = false;
		currentSpellNum = 1;
		
	}
	public SpellBase getSelectedSpell(EntityLogical ent) {
		return ent.selectedSpell;
	}
	public void tick(){
		super.tick();
		if(!hookedIntoChest) {
		for(EntityItemDrop item : game.itemsOnGround) {
			checkIfPushingItem(item);
		}
		for(Block block : game.blockList) {
			if(block instanceof BlockChest) {
				if(checkIfHittingBlock(block) && isKeyDown(KEY_E))
				{
					hookedIntoChest = true;
					((BlockChest) block).selectContainer((EntityPlayer)this);
					hookedChest=(BlockChest)block;
				}
			}
			if(block instanceof BlockNote)
			{
				if(checkIfHittingBlock(block) && isKeyDown(KEY_E))
				{
					((BlockNote)block).onRead((EntityPlayer)this);
				}
			}
			
			else checkIfHittingBlock(block);
		}
		}

	}
	public void render()
	{
		super.render(r,g,b);
	}
	public void pickItem(Item item) {
		
	}
	public void selectSpell(SpellBase spell) {
			selectedSpell = spell;
			System.out.println("[INFO] Selected Spell " + spell.name + ".");
	}
	
	public boolean checkIfHittingBlock(Block block) {
			if(bounds.min.y >= block.bounds.min.y - 64 && bounds.min.y <= block.bounds.min.y -25 && bounds.min.x <= block.bounds.min.x + 49 && bounds.min.x >= block.bounds.min.x - 49) {
				pos.y = block.pos.y - 64.1f;
				return true;
			}
			
			if(bounds.min.y <= block.bounds.min.y + 64 && bounds.min.y >= block.bounds.min.y -26 && bounds.min.x <= block.bounds.min.x + 49 && bounds.min.x >= block.bounds.min.x - 49) {

				pos.y = block.pos.y + 64.1f;
				return true;
			}
			
			if(bounds.min.x <= block.bounds.min.x + 64 && bounds.min.x >= block.bounds.min.x + 26 && bounds.min.y <= block.bounds.min.y + 49 && bounds.min.y >= block.bounds.min.y - 49){
				pos.x = block.pos.x + 64.1f;
				return true;
			}
			
			if(bounds.min.x >= block.bounds.min.x - 64 && bounds.min.x <= block.bounds.min.x + 25 && bounds.min.y <= block.bounds.min.y + 49 && bounds.min.y >= block.bounds.min.y - 49){
				pos.x = block.pos.x - 64.1f;
				return true;
			}
		return false;
	}
	public boolean checkIfPushingItem(EntityItemDrop ent) {
		if(ent.item.force < force) {
			
			
		if(bounds.min.y >= ent.bounds.min.y - 64 && bounds.min.y <= ent.bounds.min.y -25 && bounds.min.x <= ent.bounds.min.x + 49 && bounds.min.x >= ent.bounds.min.x - 49 && !touchingItemX) {
			//touchingItemY = true;
			if(!ent.touchingNorthWall){
			ent.pos.y += 1;
			}
			else pos.y = ent.pos.y - 64;
			return true;
		}
		
		if(bounds.min.y <= ent.bounds.min.y + 64 && bounds.min.y >= ent.bounds.min.y -26 && bounds.min.x <= ent.bounds.min.x + 49 && bounds.min.x >= ent.bounds.min.x - 49 && !touchingItemX) {
			//touchingItemY = true;
			if(!ent.touchingSouthWall){
				ent.pos.y -= 1;
				}
			else pos.y = ent.pos.y + 64;
			return true;
		}
		
		if(bounds.min.x <= ent.bounds.min.x + 64 && bounds.min.x >= ent.bounds.min.x + 26 && bounds.min.y <= ent.bounds.min.y + 49 && bounds.min.y >= ent.bounds.min.y - 49 && !touchingItemY){
			//touchingItemX = true;
			if(!ent.touchingWestWall) {
				ent.pos.x -= 1;
			}
			else pos.x = ent.pos.x + 64;
			return true;
		}
		
		if(bounds.min.x >= ent.bounds.min.x - 64 && bounds.min.x <= ent.bounds.min.x + 25 && bounds.min.y <= ent.bounds.min.y + 49 && bounds.min.y >= ent.bounds.min.y - 49 && !touchingItemY){
			//touchingItemX = true;
			if(!ent.touchingEastWall) {
				ent.pos.x += 1;
			}
			else pos.x = ent.pos.x - 64;
			return true;
		}
		
	}
		return false;
}
	public void regenerateVariables() {
		count++;
		if(count >= game.fps) {
			if(health < maxHealth && !isDead) {
			health++;
			}
			if(mana < maxMana && !isDead && !blockedM) {
			mana++;
		}
			count = 0;
		}
	}

}
