package main;

import org.newdawn.slick.opengl.Texture;

public class EntityMonster extends EntityLiving {

	
	public EntityMonster(String name, Game game,EnumDemeanor demeanor, Texture texture) {
		super(name, game,demeanor, texture);
		damage = 4;
		
	}
	
	public void render(){
		super.render(0,1,0);
	}
	
	public void tick(){
		super.tick();
	}
	
	

}
