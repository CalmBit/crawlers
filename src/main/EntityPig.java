package main;

import org.newdawn.slick.opengl.Texture;

public class EntityPig extends EntityMob {

	
	public EntityPig(Game game, Integer UID) {
		super("Pig" + UID, game,EnumDemeanor.PASSIVE, Game.pigTexture);
		behaviors.addBehavior(1,new EntityAIWander(1f,this));
		
	}
	
	public void render() {
		super.render(r,g,b);
	}
	
	
	public void tick()
	{
		super.tick();
	}
		
		

	
	

}
