package main;

import static org.lwjgl.input.Keyboard.KEY_E;
import static org.lwjgl.input.Keyboard.KEY_G;
import static org.lwjgl.input.Keyboard.isKeyDown;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

public class EntityPlayer extends EntityLogical {

	public MasterSpellList spellsL = new MasterSpellList(this,game);
	
	//freaking horrible hack to list spells, need a better way
	//possibly hold spells as a pre dec val?
	public SpellBase spawnMana = new SpellSpawnManaPotion(game);
	public SpellBase createWall = new SpellCreateWall(game);
	public SpellBase spawnPig = new SpellSpawnPig(game);
	public SpellBase spawnZombie = new SpellRaiseDead(game);
	public SpellBase conjureSword = new SpellConjureSword(game);
	//public SpellBase castShield = new SpellCastShield(game);

	
	public PlayerInventory inventory = new PlayerInventory(this, game);
	public boolean pickDisabled;
	public byte lastDirectionGone;
	//ugly hacks to prevent horribly rapid use
	public int useTimer;
	public int changeTimer;
	public int guiTimer;
	//true is female, false is male;
	public boolean gender;
	//a freaking hack in order to show sword path
	public boolean showToolTrail;
	public int trailTimer = 0;
	
	//soon to be active username reference
	public String username = "EB547";
	
	
	
	//start main declarations
	public EntityPlayer(Game game) {
		super("Player",game,EnumDemeanor.PASSIVE, Game.playerTexture);
		spellsLearned.put(1, spawnMana);
		spellsLearned.put(2, createWall);
		spellsLearned.put(3, spawnPig);
		spellsLearned.put(4, spawnZombie);
		spellsLearned.put(5, conjureSword);
		//spellsLearned.put(6, castShield);
		maxHealth = 64;
		health = 64;
		force = 50;
		gender = false;
		damage = 5;
		hookedIntoChest = false;
		hookedChest = null;
		if(!gender)
		{
		r = 0.65f;
		b = 1;
		g = 1;
		}
		else if(gender) {
		r = 1;
		b = .65f;
		g = .6f;
		}
		//textured = true;
		useTimer = 0;
		game.ph = health;
		pickDisabled = false;
		selectedSpell = spellsLearned.get(currentSpellNum);
		
	}

	public void pickDrop(EntityItemDrop item) {
		inventory.pickItem(new ItemStack(item.item,1));
		game.itemsToDel.add(item);
	}
	public void tick() {
			super.tick();
		
			
			if(hookedIntoChest && Keyboard.isKeyDown(Keyboard.KEY_Q))
			{
				hookedIntoChest = false;
				hookedChest.deselectContainer();
			}
			if(hookedIntoChest && isKeyDown(Keyboard.KEY_O) && changeTimer == 0 && hookedChest.selector >= 1) {
				hookedChest.selector--;
				System.out.println("[INFO] Selected Chest Slot " + hookedChest.selector);
				if(hookedChest.content[hookedChest.selector] != null)
				{
					if(hookedChest.content[hookedChest.selector].stackQ >= 2) {
					System.out.println("[INFO] This chest slot contains " + hookedChest.content[hookedChest.selector].stackQ + " of your" + hookedChest.content[hookedChest.selector].item.name + "s");
					}
					else
					{
						System.out.println("[INFO] This chest slot contains a " + hookedChest.content[hookedChest.selector].item.name);
					}
					
				}
				changeTimer = 50;
			}
			
			if(hookedIntoChest && isKeyDown(Keyboard.KEY_P) && changeTimer == 0 && hookedChest.selector <= 31) {
				hookedChest.selector++;
				System.out.println("[INFO] Selected Chest Slot " + hookedChest.selector);
				if(hookedChest.content[hookedChest.selector] != null)
				{
					if(hookedChest.content[hookedChest.selector].stackQ >= 2) {
					System.out.println("[INFO] This chest slot contains " + hookedChest.content[hookedChest.selector].stackQ + " of your" + hookedChest.content[hookedChest.selector].item.name + "s");
					}
					else
					{
						System.out.println("[INFO] This chest slot contains a " + hookedChest.content[hookedChest.selector].item.name);
					}
					
				}
				changeTimer = 50;
			}
			if(hookedIntoChest && isKeyDown(Keyboard.KEY_SPACE) && useTimer == 0)
			{
				hookedChest.insertItem(hookedChest.selector, inventory.pInv[inventory.selection]);
				useTimer = 120;
			}
			if(hookedIntoChest && isKeyDown(Keyboard.KEY_V) && useTimer == 0)
			{
				hookedChest.removeItem(hookedChest.selector);
				useTimer = 120;
			}
			
			if(!hookedIntoChest) {
			for(int i = 1;i <= spellsLearned.size();i++)
			{
					spellsLearned.get(i).tick();
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
				this.pos.y -= 1f;
				lastDirectionGone = 1;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
				this.pos.y += 1f;
				lastDirectionGone = 2;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
				this.pos.x -= 1f;
				lastDirectionGone = 3;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
				this.pos.x += 1f;
				lastDirectionGone = 4;
			}
			spells.tick();
			if(game.isDebugMode) {
				health = maxHealth;
				mana = maxMana;
			}
			regenerateVariables();
			
			if(isKeyDown(KEY_G)) {
				if(selectedSpell.curCool == 0) {
					selectedSpell.onCast(this);
				}
			}
			for(EntityItemDrop item : game.itemsOnGround) {	
				if(isKeyDown(KEY_E) && checkIfPushingItem(item)) {
				pickDrop(item);
			}
			}
			//debug enchant function
			/*if(isKeyDown(Keyboard.KEY_J))
				
			{
				if(inventory.pInv[inventory.selection] instanceof ItemStack)
				{
					if(inventory.pInv[inventory.selection].item.isTool)
					{
						game.enchantTool(inventory.pInv[inventory.selection].item,(byte)1,(byte)1, false);
					}
				}
			}*/
			if(isKeyDown(Keyboard.KEY_SPACE)) {
				if(useTimer == 0) {
				if(inventory.pInv[inventory.selection] instanceof ItemStack) {
				if(inventory.pInv[inventory.selection] != null)  {
				inventory.pInv[inventory.selection].item.onUse(this);
				if(inventory.pInv[inventory.selection].item.maxStack > 1)
				System.out.println("[INFO] You "  + inventory.pInv[inventory.selection].item.prefix + " a " +  inventory.pInv[inventory.selection].item.name + "!");
				else {
					System.out.println("[INFO] You " + inventory.pInv[inventory.selection].item.prefix + " your " +  inventory.pInv[inventory.selection].item.name + "!");
					useTimer = 50;
					showToolTrail = true;
					trailTimer = 10;
				}
			
				if(inventory.pInv[inventory.selection].item.disposable) {
					inventory.removeItem(inventory.selection);
					useTimer = 500;
				}
				}
				}
				
				else {
					System.out.println("[ERROR] Nothing in the slot to use!");
					useTimer = 50;
					}
				}
			}
			
			if(showToolTrail) {
					trailTimer--;
					glColor3f(1,1,1);
					switch(lastDirectionGone) {
					case 4:
						glBegin(GL_QUADS);
						glVertex2f(pos.x+50,pos.y);
						glVertex2f(pos.x+100,pos.y);
						glVertex2f(pos.x+100,pos.y + 50);
						glVertex2f(pos.x+50,pos.y + 50);
						glEnd();
						for(EntityLiving ent:this.game.entities)
						{
							if(ent.pos.x <= pos.x+100 && ent.pos.x >= this.pos.x + 50 && ent.pos.y <= pos.y + 50 && ent.pos.y + 50 >= pos.y)
							{
								attackEntity(ent, this, damage, pos, ent.pos);
							}
						}
						break;
					case 3:
						glBegin(GL_QUADS);
						glVertex2f(pos.x,pos.y);
						glVertex2f(pos.x-50,pos.y);
						glVertex2f(pos.x-50,pos.y + 50);
						glVertex2f(pos.x,pos.y + 50);
						glEnd();
						for(EntityLiving ent:this.game.entities)
						{
							if(ent.pos.x + 100 >= pos.x && ent.pos.x <= this.pos.x + 50 && ent.pos.y <= pos.y + 50 && ent.pos.y + 50 >= pos.y)
							{
								attackEntity(ent, this, damage, pos, ent.pos);
							}
						}
						break;
					case 2:
						glBegin(GL_QUADS);
						glVertex2f(pos.x,pos.y);
						glVertex2f(pos.x+50,pos.y);
						glVertex2f(pos.x+50,pos.y-50);
						glVertex2f(pos.x,pos.y - 50);
						glEnd();
						for(EntityLiving ent:this.game.entities)
						{
							if((ent.pos.x <= pos.x + 50 && ent.pos.x+50 >= pos.x) && ent.pos.y + 50 >= pos.y - 50 && ent.pos.y + 50 <= pos.y)
							{
								attackEntity(ent, this, damage, pos, ent.pos);
							}
						}
						break;
					case 1:
						glBegin(GL_QUADS);
						glVertex2f(pos.x,pos.y + 50);
						glVertex2f(pos.x+50,pos.y+50);
						glVertex2f(pos.x+50,pos.y+100);
						glVertex2f(pos.x,pos.y + 100);
						glEnd();
						for(EntityLiving ent:this.game.entities)
						{
							if((ent.pos.x <= pos.x + 50 && ent.pos.x+50 >= pos.x) && ent.pos.y <= pos.y + 100 && ent.pos.y + 50 >= pos.y)
							{
								attackEntity(ent, this, damage, pos, ent.pos);
							}
						}
						break;
					default:
						break;
					}
				}
		
				if(isKeyDown(Keyboard.KEY_O) && changeTimer == 0) {
					inventory.selection--;
					inventory.changeCurrent();
					changeTimer = 50;
				}
				if(isKeyDown(Keyboard.KEY_P) && changeTimer == 0) {
					inventory.selection++;
					inventory.changeCurrent();
					changeTimer = 50;
				}
			if(isKeyDown(Keyboard.KEY_SEMICOLON) && changeTimer == 0) {
				inventory.ingSlot++;
				inventory.changeIngSlot();
				changeTimer = 50;
			}
			if(isKeyDown(Keyboard.KEY_L) && changeTimer == 0) {
				inventory.ingSlot--;
				inventory.changeIngSlot();
				changeTimer = 50;
			}
			if(isKeyDown(Keyboard.KEY_PERIOD) && getSelectedSpell(this).curCool == 0 && changeTimer == 0 && currentSpellNum >= 2 && currentSpellNum <= spellsLearned.size())
			{
				currentSpellNum--;
				System.out.println("Spell Num: " + currentSpellNum + " Spell List Size: " + spellsLearned.size());
				selectSpell(spellsLearned.get(currentSpellNum));
				changeTimer = 50;
			}
			if(isKeyDown(Keyboard.KEY_SLASH) && getSelectedSpell(this).curCool == 0 && changeTimer == 0 && currentSpellNum >= 1 && currentSpellNum <= spellsLearned.size() - 1)
			{
				currentSpellNum++;
				System.out.println("Spell Num: " + currentSpellNum + " Spell List Size: " + spellsLearned.size());
				selectSpell(spellsLearned.get(currentSpellNum));
				changeTimer = 50;
			}
			if(isKeyDown(Keyboard.KEY_B) && guiTimer <= 0)
			{
				guiTimer = 50;
				game.pGui.craftDisplay = !game.pGui.craftDisplay;
				
			}
			}
			
			useTimer--;
			if(useTimer <= 0) {
				useTimer = 0;
			}
			changeTimer--;
			if(changeTimer <= 0) {
				changeTimer = 0;
			}
			inventory.tick();
			
			if(guiTimer <= 0)
			{
				guiTimer = 0;
			}
			else
			{
				guiTimer--;
			}
			if(trailTimer <= 0)
			{
				showToolTrail = false;
			}
			game.px = pos.x;
			game.py = pos.y;
			game.ph = health;
			game.pm = mana;
			game.cc = getSelectedSpell(this).curCool;
			game.ut = useTimer;
			
	}
	
	
	public void toolJunk(ItemTool tool)
	{
		tool.onUse(this);
	}
	
	public void render() {
		super.render(r,g,b);
		
	}
	
	
}