package main;

public class EntityZombie extends EntityMonster {

	protected EntityAIBehaviors behaviors = new EntityAIBehaviors();
	public EntityAIAttack attack = new EntityAIAttack(this,1);
	public EntityAISight sight = new EntityAISight(this);
	public EntityZombie(String name, Game game) {
		super(name, game,EnumDemeanor.AGRESSIVE, game.zombieTexture);
		behaviors.addBehavior(1,attack);
		behaviors.addBehavior(1,sight);
		behaviors.executeBehavior(attack);
		behaviors.executeBehavior(sight);
		health = 50;
		damage = 2;
	}
	public void tick()
	{
		super.tick();
		behaviors.updateExecutingBehaviors();
	}
	public void onAttacked()
	{
		super.onAttacked();
	}

}
