package main;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;

import java.awt.Font;
import org.newdawn.slick.TrueTypeFont;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.rmi.server.UID;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Set;

import javax.security.auth.x500.X500Principal;
import javax.xml.crypto.dsig.keyinfo.PGPData;

import main.gui.ButtonMenuAction;
import main.gui.ButtonReturnBoolean;
import main.gui.GuiHandle;
import main.gui.InterfaceSelectable;
import main.gui.PlayerGUI;
import main.gui.TextBoxMain;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import paulscode.sound.SoundSystem;



import static org.lwjgl.input.Keyboard.*;


public class Game {
	public String name;
	public SaveGame saver = new SaveGame();
	public EntityPlayer player;
	public PlayerGUI pGui;
	public Set<EntityLiving> entities = new HashSet<EntityLiving>();
	public Set<Entity> deleted = new HashSet<Entity>();
	public Set<EntityItemDrop> itemsOnGround = new HashSet<EntityItemDrop>();
	public Set<EntityItemDrop> itemsToDel = new HashSet<EntityItemDrop>();
	public Set<Block> blockList = new HashSet<Block>();
	public Random entitynumber;
	public int UID = 1;
	public static boolean isDebugMode = true;
	public boolean saveReady = true;
	public int saveCounter;
	public boolean spawnReady = true;
	public int spawnCounter;
	public int screenX = 1024;
	public int screenY = 768;
	public float px;
	public float py;
	public float ph;
	public int pm;
	public int cc;
	public int ut;
	public boolean wt;
	public int state = 2;
	public int buttonTimer = 0;
	public int fpsDisp;
	public TextBoxMain currentlySelected;
	
	public TextBoxMain worldname = new TextBoxMain(100,100,400,25,this);
	//menu buttons
	public ButtonReturnBoolean quit = new ButtonReturnBoolean("Quit", screenX/4, 600, 256.0F, 128.0F, true, this, quitButtonTexture);
	public ButtonMenuAction newg = new ButtonMenuAction("New Game", screenX/4, 300, 512.0F, 128.0F, true, 5, this, newButtonTexture);
	public ButtonMenuAction loadg = new ButtonMenuAction("Load Game", screenX/4, 450, 512.0F, 128.0F, true, 3, this, loadButtonTexture);
	public ButtonMenuAction settings = new ButtonMenuAction("Settings", screenX/2, 600, 256.0F, 128.0F, true, 4, this, settingsButtonTexture);

	//extra buttons
	public ButtonMenuAction returntom = new ButtonMenuAction("Return To Menu", screenX/4, 500, 512.0F, 128.0F, true, 0, this, rtmButtonTexture);
	public ButtonMenuAction startg = new ButtonMenuAction("Start Game", screenX/4, 300, 512.0F, 128.0F,true,1,this,startButtonTexture);
	public static EnumGameState GameState;
	public String unitAmount = "Oz";
	public String version ="Alpha 0.1.0 PRE-RELEASE 3";
	public boolean isQuitRequested;
	public AngelCodeFont font;
	public AngelCodeFont notefont;
	//texture declearations
	public static Texture playerTexture;
	public static Texture healthTexture;
	public static Texture healthBackTexture;
	public static Texture manaTexture;
	public static Texture loadButtonTexture;
	public static Texture newButtonTexture;
	public static Texture quitButtonTexture;
	public static Texture settingsButtonTexture;
	public static Texture rtmButtonTexture;
	public static Texture startButtonTexture;
	public static Texture pigTexture;
	public static Texture swordTexture;
	public static Texture bottleTexture;
	public static Texture zombieTexture;
	public static Texture greendustTexture;
	public static Texture shieldTexture;
	public static Texture chestTexture;
	public static Texture logoTexture;
	public static Texture noteTexture;
	public static Texture splashTexture;
	JSONObject ents = new JSONObject();
	JSONObject blocks = new JSONObject();
	SoundSystem sound = new SoundSystem();

	BlockChest chest;
	
	int fps;
	long lastFrame;
	long lastFPS;
	
	
	
	public void addEntity(EntityLiving ent) {
		entities.add(ent);
	}
	
	public void init() throws IOException
	{
		playerTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("player.png"), "player.png");
		healthTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("healthgreen.png"), "healthgreen.png");
		healthBackTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("healthred.png"),"healthred.png");
		manaTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("manablue.png"),"manablue.png");
		//playButtonTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("playbuttontex.png"),"playbuttontex.png");
		loadButtonTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("loadgamebutton.png"),"loadgamebutton.png");
		quitButtonTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("quitbuttontex.png"),"quitbuttontex.png");
		settingsButtonTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("settingsbuttontex.png"),"settingsbuttontex.png");
		newButtonTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("newgamebutton.png"),"newgamebutton.png");
		rtmButtonTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("rtmbutton.png"), "rtmbutton.png");
		startButtonTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("startbuttontex.png"),"startbuttontex.png");
		logoTexture = addTexture("PNG",ResourceLoader.getResourceAsStream("ctodlogo.png"),"ctodlogo.png");
		loadg.texture = loadButtonTexture;
		newg.texture = newButtonTexture;
		quit.texture = quitButtonTexture;
		settings.texture = settingsButtonTexture;
		returntom.texture = rtmButtonTexture;
		startg.texture = startButtonTexture;
		pigTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("pig.png"), "pig.png");
		swordTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("sword.png"), "sword.png");
		bottleTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("bottle.png"), "bottle.png");
		zombieTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("zombie.png"), "zombie.png");
		greendustTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("greendust.png"), "greendust.png");
		shieldTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("shield.png"),"shield.png");
		splashTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("splash.png"), "splash.png");
		//temp tile before using sheet
		chestTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("chest.png"),"chest.png");
		noteTexture = addTexture("PNG", ResourceLoader.getResourceAsStream("note.png"),"note.png");
		sound.backgroundMusic("Music", "earthday.midi", true);
		//font
		try {
			font = new AngelCodeFont("gamfnt.fnt", "gamfnt_0.tga");
			notefont = new AngelCodeFont("notefont.fnt", "notefont_0.tga");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//map inits
		addBlock(new BlockNote(Material.paper,0,2,this,"This Should Be a Full Line, me thinks.", "As should this, says I.", "And this ya blarny men!", "Yar-Har", "Yo-Ho", "Sincerly,", "-Crazed Pirate Brooks"),new Vector2f(400f,400f));
		chest = new BlockChest(Material.wood,0,2,this);
		ItemStack sword = new ItemStack(Item.gemSword,1);
		//enchantTool(sword.item,(byte)1,(byte)2,true);
		addBlock(chest,new Vector2f(200,200));
		chest.createItem(0, sword);
	}
	public Texture addTexture(String format, InputStream stream, String path)
	{
		try {
		if(isDebugMode) {
		System.out.println("[DEBUG]: File " + path + " was loaded.");
		}
		return TextureLoader.getTexture(format,stream);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
		}
	
	public void addDrop(EntityItemDrop item) {
		itemsOnGround.add(item);
	}
	public void addBlock(Block block,Vector2f pos) {
		block.pos = pos;
		blockList.add(block);
	}
	public void setDeath(Entity ent) {
		if(ent.health <= 0) {
			ent.isDead = true;
			ent.health = 0;
		}
	}

	public void splashRender()
	{
		splashTexture.bind();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0,0);
		GL11.glVertex2f(0, 0);
		GL11.glTexCoord2f(1,0);
		GL11.glVertex2f((float) (1024*1.45), 0);
		GL11.glTexCoord2f(1,1);
		GL11.glVertex2f((float) (1024*1.45), (float) (768*1.75));
		GL11.glTexCoord2f(0,1);
		GL11.glVertex2f(0, (float) (768*1.75));
		GL11.glEnd();
	}
	public void removeEntity(Entity ent) {
		spawnReady = false;
		entities.remove(ent);
		if(spawnCounter >= 5000)
		spawnReady = true;
	}
	
	public void constantTicks()
	{
		if(buttonTimer >= 1) buttonTimer--;
	}
	
	public void tick() {
		font.drawString(screenX-100, 0,"FPS:" + Integer.toString(fpsDisp));
		if(isKeyDown(KEY_F)) {
			if(saveReady) {
				saveReady=false;
				saver.startSaving(this);
				for(Entity ent : entities) {	
					saver.saveEntity(ent);
				}
				for(Block block : blockList)
				{
					saver.saveBlock(block);
				}
				saveCounter = 0;
				saver.endSaving();
			}
		}
		saveCounter++;
		spawnCounter++;
		if(saveCounter >= 5000) {
			saveReady = true;
		}
		if(spawnCounter >= 5000) {
			spawnReady = true;
		}
		
		if(isKeyDown(Keyboard.KEY_F8))
		{
			
			addEntity(new EntityZombie("Zombie", this));
			
		}
		for(EntityLiving ent : entities) {
			ent.tick();
			if(ent.health <= 0) 
				{
				System.out.println("[INFO] " + ent.name + " has died.");
				deleted.add(ent);
				}
			}
		
		
		for(EntityItemDrop item : itemsOnGround) {
			item.tick();
		}
		
		
		for(Entity ent : deleted) {
			entities.remove(ent);
		}
		
		for(EntityItemDrop item : itemsToDel) {
			itemsOnGround.remove(item);
		}
		for(Block block : blockList) {
			block.tick();
		}
		if(isKeyDown(KEY_N)) {
			isDebugMode = true;
		}
		if(isKeyDown(KEY_ESCAPE)) {
			isDebugMode = false;
		}
		pGui.render();
	}
	
	 public void settingsScreen()
	 {
		 returntom.render();
		 returntom.update();
	 }
	 
	 public void loadGameScreen()
	 {
		 returntom.render();
		 returntom.update();
	 }
	
	 public void newGameScreen()
	 {
		 font.drawString(100, 70, "World Name:");
		 if(name != null) {
		 font.drawString(100, 150, "Location: " + name);
		 }
		 font.drawString(100,200, "Debug: " + isDebugMode);
		 startg.render();
		 returntom.render();
		 startg.update();
		 returntom.update();
		 worldname.render();
		 worldname.update();
		 
	 }
	public void enchantTool(Item item, byte type, byte level, boolean silent)
	{
		if(item.enchantmentList.isEmpty()) {
		switch(type)
		{
		case 1:
			EnchantmentBase enchant1 = new EnchantmentSharpness(item,level);
			item.enchantmentList.add(enchant1);
			if(!silent)
			System.out.println("[INFO] Enchant Sharpness Added!");
					
			item.name = "Sharp " + item.name;
			break;
		case 2:
			EnchantmentBase enchant2 = new EnchantmentFortune(item,level);
			item.enchantmentList.add(enchant2);
			if(!silent)
			System.out.println("[INFO] Enchant Fortune Added!");
			item.name = "Fortunate " + item.name;
			break;
		default:
			break;
		}
		switch(level)
		{
		case 1:
			item.r = 1;
			item.b = 0;
			item.g = 0;
			break;
		case 2:
			item.r = 1;
			item.b = 1;
			item.g = 0;
			break;
		case 3:
			item.r = 1;
			item.b = 1;
			item.g = 1;
			break;
		default:
			break;
		}
		}
	}
	public void doNothing() {
		
	}
	
	
	 public long getTime() {
		    return (Sys.getTime() * 1000) / Sys.getTimerResolution();
		}
	 
	 public void collisions() {
		
	 }
	 
	 public int mouseX()
	 {
		 return  Mouse.getX();
	 }
	 public int mouseY()
	 {
		 return Math.abs(Mouse.getY() - screenY);
	 }
	 public void cleanUp(){
		 for (Entity ent : this.entities) {
		      ent.cleanup();
		      if(isDebugMode) {
		      System.out.println("[DEBUG] " + ent.name + " cleaned up");
		      }
		    }
		 	sound.cleanup();
		    System.out.println("[INFO] Thanks For Playing " + this.version + " of Crawlers-Tombs Of Darkness! We hope you enjoyed it!");
	 }
	 public void menuUpdate() {
			this.loadg.render();
			this.newg.render();
		    this.quit.render();
		    this.settings.render();
		    Color.darkGray.bind();
		    logoTexture.bind();
		    GL11.glBegin(GL11.GL_QUADS);
		    GL11.glTexCoord2f(0, 0);
		    GL11.glVertex2f(screenX/4, 100);
		    GL11.glTexCoord2f(1, 0);
		    GL11.glVertex2f(screenX/4 + 512, 100);
		    GL11.glTexCoord2f(1, 1);
		    GL11.glVertex2f(screenX/4 + 512, 100 + 128);
		    GL11.glTexCoord2f(0, 1);
		    GL11.glVertex2f(screenX/4, 100 + 128);
		    GL11.glEnd();
		    
		    this.loadg.update();
		    this.newg.update();
		    this.quit.update();
		    this.settings.update();
		    if (this.quit.returnB)
		    {
		      this.isQuitRequested = true;
		    }
			
		}
	public Game(String name) {
		this.name = name;
		if(isDebugMode) {
		System.out.println("[DEBUG] Crawlers Version " + version);
		System.out.println("[DEBUG] Agelastic Studios (C) 2012");
		System.out.println("[DEBUG] Current World Name: " + name);
		}
		pGui = new PlayerGUI(this,(new PlayerInventory(player, this)));
	}

	

}
