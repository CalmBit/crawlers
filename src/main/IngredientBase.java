package main;

public abstract class IngredientBase {

	public String name;
	public int ID;
	public float ratio;
	public IngredientBase(String name, int ID,float ratio)
	{
		this.name = name;
		this.ID = ID;
		this.ratio = ratio;
		
	}
}
