package main;

public class IngredientStack {

	public IngredientBase in;
	public float amount;
	
	public IngredientStack(IngredientBase in, float f)
	{
		this.in = in;
		this.amount = f;
	}
}
