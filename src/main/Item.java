package main;

import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.opengl.Texture;

public class Item {
	
	public static Map<Integer, Item> itemsl = new HashMap<Integer, Item>();
	public static Item emptyBottle = new ItemBottle(1,"Glass Bottle",0, Game.bottleTexture);
	public static Item healthPotion = new ItemBottle(2, "Health Potion", 1, Game.bottleTexture);
	public static Item manaPotion = new ItemBottle(3, "Mana Potion", 2, Game.bottleTexture);
	public static Item woodSword = new ItemSword(4, "Wood Sword",5,EnumToolMaterials.WOOD, Game.swordTexture);
	public static Item stoneSword = new ItemSword(5, "Stone Sword",5,EnumToolMaterials.STONE, Game.swordTexture);
	public static Item ironSword = new ItemSword(6, "Iron Sword",5,EnumToolMaterials.IRON, Game.swordTexture);
	public static Item gemSword = new ItemSword(7, "Gem Sword",5,EnumToolMaterials.GEM, Game.swordTexture);
	public static Item soulSword = new ItemSword(8, "Soul Sword",5,EnumToolMaterials.SOUL, Game.swordTexture);
	public static Item testSword = new ItemSword(9, "TEST", 0, EnumToolMaterials.TEST, Game.swordTexture);
	public static Item greenGunpowder = new ItemDynamic(10, "Green Gunpowder", 0, 0, 0, 0, IngredientDeclerationContainer.gunpowder, IngredientDeclerationContainer.greendust, null, 2, Game.greendustTexture);
	
	
	public String name;
	public int ID;
	int metadeta = 0;
	public int force;
	public int r,b,g;
	public String prefix;
	public int damage;
	public boolean disposable;
	public int maxStack;
	public boolean destroy;
	public boolean isTool;
	public EnumToolMaterials material;
	public List<EnchantmentBase> enchantmentList = new ArrayList<EnchantmentBase>();
	public Texture dropTexture;
	
	public Item(int index,String name, int damage,int maxStack, Texture texture)
	{
		this.name = name;
		this.maxStack = maxStack;
		dropTexture = texture;
		ID = index;
		//itemsl.put(ID, this);
		force = 1;
		if(prefix == null)
		{
			prefix = "used";
		}
		destroy = false;
		
		/*itemL[1] = emptyBottle;
		itemL[2] = healthPotion;
		itemL[3] = manaPotion;*/
	}
	public Item(int index,String name, int damage,int maxStack,EnumToolMaterials material, Texture texture)
	{
		this(index,name,damage,maxStack, texture);
		this.material = material;
	}
	public Item cloneItem()
	{
		Item newItem = new Item(ID,name,damage, maxStack, dropTexture);
		newItem.metadeta = metadeta;
		newItem.force = force;
		newItem.r = r;
		newItem.b = b;
		newItem.g = g;
		newItem.disposable = disposable;
		newItem.destroy = destroy;
		newItem.material = material;
		newItem.prefix = prefix;
		return newItem;
	}
	public Item(int index,String name,Texture texture) {
		this(index, name, 0,64, texture);
	}
	public Item(int index,String name) {
		this(index, name,0,64, null);
	}
	
	public void onUse(EntityPlayer ent) {
		if(isTool)
		{
			
			if(metadeta < material.durability)
			{
				metadeta++;
				System.out.println("[INFO] Damage:" + metadeta + ",Damage Left:" + (material.durability - metadeta));
				ent.useTimer = 50;
				if(metadeta == material.durability)
				{
					onBreak();
				}
			}
			
			else {
				onBreak();
			}
		}
	}
	public void onBreak()
	{
		System.out.println("[INFO] Your " + name + " has broken!");
		destroy = true;
	}
	
}
