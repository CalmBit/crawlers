package main;

import org.newdawn.slick.opengl.Texture;

public class ItemBottle extends Item {

	
	public ItemBottle(int index, String name,int meta, Texture texture) {
		
		super(index,name, texture);
		metadeta = meta;
		if(meta == 1) {
			r = 1;
			b = 0;
			g = 0;
			prefix = "drank";
		}
		if(meta == 2) {
			r = 0;
			b = 1;
			g = 0;
			prefix = "drank";
		}
		disposable = true;
	}
	public void onUse(EntityPlayer ent) {
	super.onUse(ent);
		if(metadeta == 1) {
			if(ent.health < ent.maxHealth){
				if(ent.health + 50 > ent.maxHealth) {
					ent.health = ent.maxHealth;
				}
				else ent.health += 50;
			}
			
		}
		else if(metadeta == 2) {
			if(ent.mana < ent.maxMana) {
			if(ent.mana + 50 > ent.maxMana) {
				ent.mana = ent.maxMana;
			}
			else ent.mana += 50;
			}
		}
		else;
		ent.pickItem(this);
	}
	


}
