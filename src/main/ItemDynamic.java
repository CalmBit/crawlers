package main;

import org.newdawn.slick.opengl.Texture;

public class ItemDynamic extends Item {

	public float ratio;
	public float q1;
	public float q2;
	public float q3;
	public IngredientBase in1;
	public IngredientBase in2;
	public IngredientBase in3;
	//total amount is the ingredients ratio times its quantity added together times the ratio of the item
	//eg. 1 oz. gunpowder (ratio .5) and 1 oz. of greendust (ratio .25 [potent stuff!]) = total .75 oz (1.50 oz. ratio applyed) of greengunpowder 
	public int amount;
	public ItemDynamic(int index, String name, int damage, Texture texture) {
		super(index, name, damage, 100000, texture);
		// TODO Auto-generated constructor stub
	}
	public ItemDynamic(int index, String name, int damage, int q1, int q2, int q3, IngredientBase in1, IngredientBase in2, IngredientBase in3, int ratio, Texture texture) {
		this(index, name, damage, texture);
		this.q1 = q1;
		this.q2 = q2;
		this.q3 = q3;
	}

}
