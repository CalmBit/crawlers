package main;




public class ItemStack {
	public int stackQ;
	public int itemID;
	public int meta;
	public String name;
	public Item item;
	public RenderItem render;
	
	public ItemStack(Item item, int Q) {
		this(Q, item);
	}
	public ItemStack(int q, Item item) {
		stackQ = 0;
		itemID = item.ID;
		name = item.name;
		meta = item.metadeta;
		stackQ = q;
		this.item = item;
		
	}
	

	
	public Item getItem() {
		return item;
	}
}
