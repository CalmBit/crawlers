package main;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.opengl.Texture;


public class ItemTool extends Item {

	EnumToolMaterials toolMaterial;

	public ItemTool(int index, String name,int damage, EnumToolMaterials material, Texture texture) {
		super(index, name, damage,1,material, texture);
		toolMaterial = material;
		disposable = false;
		isTool = true;
		prefix = "took a swing with";
	}
	
	public void onUse(EntityPlayer ent)
	{
		
	}

}
