package main;

public class MasterSpellList {
	public Game game;
	public EntityLogical entityLogical;
	
	public SpellBase spawnMana = new SpellSpawnManaPotion(game);
	
	
	public MasterSpellList(EntityLogical ent, Game game) {
		this.game = game;
		entityLogical = ent;
	}
	
	public void addSpell(SpellBase spell, EntityLogical ent) {
		ent.spells.addSpell(spell);
	}
}
