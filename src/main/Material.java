package main;

public enum Material {
rock(3,3),
air(0,0),
wood(2,2),
metal(4,5),
dirt(1,1),
bedrock(6,6),
paper(0,1);

public int resistance;
public int hardness;

private Material(int hardness, int resistance) {
	this.resistance = resistance;
	this.hardness = hardness;
}

static {
}

}
