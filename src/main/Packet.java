package main;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Packet {

	private static Map packetClassMap = new HashMap();
	
	private static Set clientPacketsSet = new HashSet();
	private static Set serverPacketSet = new HashSet();
	
	public Packet() {
		
	}
	
	
	
	
	
	static void addPacketToMap(int ID, boolean client,boolean server,Class packetClass)
	{
		if(packetClassMap.containsKey(ID))
		{
			System.out.println("[ERROR] Packet ID already listed - " + ID);
		}
		if(packetClassMap.containsValue(packetClass))
		{
			System.out.println("[ERROR] Packet already in list - " + packetClass.getName());
		}
		
		packetClassMap.put(packetClass, ID);
		
	}
	
	
	
	public Packet getNewPacket(int pID)
	{
		try {
			Class classPacket = (Class)packetClassMap.get(pID);
			
			if(classPacket == null)
			{
				return null;
			}
			else 
			{
				return (Packet)classPacket.newInstance();
			}
		}
		catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("[ERROR] Skipped a packet - ID called is " + pID);
		return null;
	}
	
	
	static {
		
		addPacketToMap(1,true,true,Packet001Login.class);
	}
	
	public void onSent(int sentToID)
	{
		
	}
	
	
}
