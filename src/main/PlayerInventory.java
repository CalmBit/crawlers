package main;

import java.util.HashSet;
import java.util.Set;

import javax.tools.Tool;


public class PlayerInventory {
	public ItemStack[] pInv;
	public IngredientStack[] pIng;
	public Set<ItemStack> toDel = new HashSet<ItemStack>();
 	public int selection;
 	public int ingSlot;
	public EntityPlayer player;
	public Game game;
	
	public PlayerInventory(EntityPlayer player, Game game) {
		this.player = player;
		this.game = game;
		pInv = new ItemStack[11];
		pIng = new IngredientStack[32];
	}
	
	public void tick() {
		
		for (int i = 0; i <= selection; i++) {
			if(pInv[i] != null)
			{
				if(pInv[i].item.destroy)
				{
					removeItem(i);
				}
			}
		}
		/*
		{
			ItemTool tool = (ItemTool)pInv[selection].item;
			if(tool.metadeta >= tool.toolMaterial.durability)
			{
				
				removeItem();
			}
		}*/
		
	}
	public void changeCurrent(){
		if(!(selection < 0) && !(selection > 9)) {
			System.out.println("[INFO] Selected slot " + selection);
			if((pInv[selection] instanceof ItemStack)) {
				pInv[selection].meta = pInv[selection].item.metadeta;
				System.out.println("[INFO] This slot contains your " + pInv[selection].item.name + "!");
				System.out.println("[INFO] You have " + pInv[selection].stackQ + " of these.");
			}
		}
		if(selection > 9) {
			selection = 9;
		}
		if(selection < 0) {
			selection = 0;
		}
	}

	public void dropItem() {
		if(pInv[selection] instanceof ItemStack) {
			game.addDrop(new EntityItemDrop(pInv[selection].item, game, pInv[selection].item.dropTexture));
			System.out.println("[INFO] Dropped " + pInv[selection].item.name);
			pInv[selection] = null;
		}
		else System.out.println("[ERROR] Nothing in this slot to drop!");
	}
	
	public void removeItem(int i) {
		if((pInv[i] instanceof ItemStack)) {
			if(pInv[i].stackQ > 1) {
				pInv[i].stackQ--;
			}
			else pInv[i] = null;
		}
	}
	
	public void pickItem(ItemStack item){
		if(pInv[selection] instanceof ItemStack && pInv[selection].stackQ < item.item.maxStack) {
		
			if(pInv[selection].itemID == item.itemID) {
			pInv[selection].stackQ++;
			System.out.println("[INFO] You picked up another " + pInv[selection].item.name + "!");
			System.out.println("[INFO] You now have " + pInv[selection].stackQ + ".");
		}
		else if(pInv[selection].stackQ < item.item.maxStack)  {
			dropItem();
			item.item = item.item.cloneItem();
			pInv[selection] = item;
			System.out.println("[INFO] You picked up a " + pInv[selection].item.name + "!");
		}
		}
		else {
			pInv[selection] = item;
			System.out.println("[INFO] You picked up a " + pInv[selection].item.name + "!");
		}
	}
	
	public void changeIngSlot()
	{
		if(ingSlot > 0 && ingSlot < 30)
		{
			System.out.println("[INFO] Ingredient Slot " + ingSlot + " was selected from your ingredients pouch.");
			if(pIng[ingSlot] instanceof IngredientStack)
			{
				System.out.println("[INFO] This Slot Contains " + pIng[ingSlot].amount + game.unitAmount + " of " + pIng[ingSlot].in.name);
			}
			else
			{
				System.out.println("[INFO] This Slot is Currently Vacant.");
			}
		}
	}
}
