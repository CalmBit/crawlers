package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public abstract class Render {

	public String path;
	
	public BufferedImage loadTexture(String path) throws IOException
	{
		BufferedImage img = ImageIO.read(new File(path));
		return img;
		
	}
}
