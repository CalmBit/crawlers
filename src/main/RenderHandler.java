package main;

public class RenderHandler {
	Game rengame;
	public RenderHandler(Game game) {
		rengame = game;
	}
	
	public void render() {
		for(Entity ent : rengame.entities) {
			ent.render(ent.r,ent.g,ent.b);
		}
		for(EntityItemDrop item : rengame.itemsOnGround) {
			item.render(item.r, item.g, item.b);
		}
		
	}
	
}
