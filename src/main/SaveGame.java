package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SaveGame {
	File filer;
	Game game;
		public SaveGame() {
			
		}
	public void startSaving(Game game)
	{
		this.game = game;
		filer = new File(System.getenv("PROGRAMFILES") + "/crawlers/saves/" + game.name + ".tgd");
		try {
			if(!filer.exists()) {
			filer.createNewFile();
			System.out.println("File Created!");
			}
			else 
			{
				filer.delete();
				filer.createNewFile();
			System.out.println("File Rewritten!");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	public void saveEntity(Entity ent) {
		game.ents.put("Name", ent.name);
		game.ents.put("Pos", ent.pos);
		game.ents.put("Health", ent.health);
		try {
			PrintWriter file = new PrintWriter(new FileWriter(filer,true));
			file.write(game.ents.toJSONString());
			file.flush();
			file.close();
			System.out.println(game.ents);
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void saveBlock(Block block)
	{
		game.blocks.put("Pos", block.pos);
		try {
		PrintWriter file = new PrintWriter(new FileWriter(filer,true));
		file.write(game.blocks.toJSONString());
		file.flush();
		file.close();
		System.out.println(game.blocks);
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		
	}
	public void endSaving()
	{
		
		for(int i = 0; i <= 300;i++)
		{
			game.font.drawString(game.screenX/4, 100, "Game Saved...");
		}
	}
	
}
