package main;

import static org.lwjgl.opengl.GL11.*;
public class SaveIndicator {
	public Game game;
	public float r,g,b;
	public SaveIndicator(Game game) {
		this.game = game;
	}
	public void render() {
		if(game.saveReady) {
			r = 0;
			g = 1;
			b = 0;
		}
		else  {
			r = 1;
			g = 0;
			b = 0;
		}
		glColor3f(r,g,b);
		glBegin(GL_QUADS);
		glVertex2f(100f,100f);
		glVertex2f(200f,100f);
		glVertex2f(200f,200f);
		glVertex2f(100f,200f);
		glEnd();
	}

}
