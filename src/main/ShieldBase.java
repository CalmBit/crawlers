package main;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.Color;

public class ShieldBase {

	public EntityLiving ent;
	public Game entgame;
	public int lifeLeft;
	
	//touch boolean array (0 left 1 right 2 up 3 down)
	public boolean touch[] = new boolean[4];
	
	public float x,y,sx,sy;
	public ShieldBase(EntityLiving ent)
	{
		this.ent = ent;
		entgame = ent.game;
		x = ent.pos.x - 64;
		sx = 192;
		y = ent.pos.y - 64;
		sy = 192;
	}
	
	public void render()
	{
		Color.white.bind();
		Game.shieldTexture.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0,1);
		glVertex2f(x,y);
		glTexCoord2f(1,1);
		glVertex2f(x + sx, y);
		glTexCoord2f(1,0);
		glVertex2f(x + sx, y + sy);
		glTexCoord2f(0,0);
		glVertex2f(x, y + sy);
		glEnd();
	}
	
	public void tick()
	{
		/*if(lifeLeft <= 0)
		{
			destroyShield();
		}*/
		x = ent.pos.x - 64;
		y = ent.pos.y - 64;
		for(EntityLiving tres : entgame.entities)
		{
			if(tres != ent) {
				if(tres.pos.x+64 >= x && (tres.pos.y <= y+sy) && (tres.pos.y + 64 >= y))
				{
					touch[0] = true;
				}
				else if(tres.pos.x <= x+sx && (tres.pos.y <= y+sy) && (tres.pos.y + 64 >= y))
				{
					touch[1] = true;
				}
				else if(tres.pos.y + 64 >= y && (tres.pos.x+64 >= x) && (tres.pos.x <= x+sx))
				{
					touch[2] = true;
				}
				else if(tres.pos.y <= y+sy && (tres.pos.x+64 >= x) && (tres.pos.x <= x+sx))
				{
					touch[3] = true;
				}
				else
				{
					for(int i = 0; i < touch.length;i++)
					{
						if(touch[i] != false)
						{
							touch[i] = false;
						}
					}
				}
				
				if(touch[0])
				{
					tres.pos.x = (int)(x-65);
				}
				if(touch[1])
				{
					tres.pos.x = (int)(x+sx-1);
				}
				if(touch[2])
				{
					tres.pos.y = (int)(y+sy-1);
				}
				if(touch[3])
				{
					tres.pos.y = (int)(y+65);
				}
			}
		}
	}
	
	public void destroyShield()
	{
		ent.shield = null;
	}
}

