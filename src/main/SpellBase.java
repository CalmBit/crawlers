package main;

public class SpellBase {

	
	
	public String name;
	public Game game;
	public int cost;
	public int coolDown;
	public int curCool;
	
	public SpellBase(String name, Game game) {
		this.name = name;
		this.game = game;
		cost = 0;
		coolDown = 0;
		curCool = 0;
	}
	
	public void tick() {
		if(curCool <= coolDown && !(curCool <= 0)) {
			curCool--;
		}
	}
	
	public void onCast(EntityLogical caster){
		if(caster.mana >= cost && !caster.isDead && curCool == 0) {
			caster.mana -= cost;
			effect();
			System.out.println("[INFO] You casted " + name + "!");
		}
		curCool = coolDown;
	}
	public void effect() {
		
	}
}
