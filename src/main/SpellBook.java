package main;

import static org.lwjgl.input.Keyboard.KEY_G;
import static org.lwjgl.input.Keyboard.isKeyDown;

import java.util.HashSet;
import java.util.Set;

public class SpellBook {

	public Set<SpellBase> spellList = new HashSet<SpellBase>();
	public Entity keeper;
	public SpellBook(Entity ent, Game game) {
		keeper = ent;
	}
	
	public void tick() {
		for(SpellBase spell : spellList) {
			spell.tick();
		}
	}
	
	public void cast(SpellBase spell,EntityLogical caster) {
		if(spellList.contains(spell)) {
			spell.onCast(caster);
		}
	}
	
	public void addSpell(SpellBase spell){
		if(!spellList.contains(spell)) {
		spellList.add(spell);
		System.out.println("[DEBUG] Spell " + spell.name + " added to " + keeper.name);
		}
		else System.out.println("[DEBUG]" + keeper.name + " already knows " + spell.name);
				
	}
	
}
