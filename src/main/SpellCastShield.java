package main;

public class SpellCastShield extends SpellBase {

	public SpellCastShield(Game game) {
		super("Cast Shield", game);
		cost = 100;
		coolDown = 1000;
	}
	
	public void effect()
	{
		ShieldBase shield = new ShieldBase(game.player);
		game.player.shield = shield;
	}

}
