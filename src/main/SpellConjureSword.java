package main;

public class SpellConjureSword extends SpellBase {

	public SpellConjureSword(Game game) {
		super("Conjure Sword", game);
		cost = 50;
		coolDown = 1000;
	}
	
	public void onCast(EntityLogical caster)
	{
		super.onCast(caster);
	}
	public void effect()
	{
		Item itemDrop = Item.woodSword.cloneItem();
		itemDrop.isTool = true;
		game.addDrop(new EntityItemDrop(itemDrop, game, itemDrop.dropTexture));
	}

}
