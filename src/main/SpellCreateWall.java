package main;

import java.util.Random;

import org.lwjgl.util.vector.Vector2f;

public class SpellCreateWall extends SpellBase {

	public Random random = new Random();
	public SpellCreateWall(Game game) {
		super("Create Wall", game);
		coolDown = 50;
		curCool = 0;
	}
	public void effect() {
		for(int i = 1; i < 4;i++) {
			game.addBlock(new BlockWall(Material.rock, 0, 1,game), new Vector2f(game.px + 50,game.py + (50*i)));
		}
	}

}
