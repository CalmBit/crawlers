package main;

public class SpellRaiseDead extends SpellBase {

	public SpellRaiseDead(Game game) {
		super("Raise Dead", game);
		cost = 100;
		coolDown = 100;
	}
	public void onCast(EntityLogical caster)
	{
		super.onCast(caster);
	}
	public void effect()
	{
		game.addEntity(new EntityZombie("Zombie" + game.UID,game));
	}

}
