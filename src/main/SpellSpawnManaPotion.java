package main;


public class SpellSpawnManaPotion extends SpellBase {
	public SpellSpawnManaPotion(Game game) {
		super("Spawn Potion", game);
		cost = 50;
		coolDown = 1000;
	}
	
	public void onCast(EntityLogical caster) {
		super.onCast(caster);
		
	}
	public void effect() {
		Item itemDrop = Item.manaPotion.cloneItem();
		game.addDrop(new EntityItemDrop(Item.manaPotion, game, ItemTool.manaPotion.dropTexture));
	}
}


