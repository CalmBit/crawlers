package main;

public class SpellSpawnPig extends SpellBase {
	public SpellSpawnPig(Game game) {
		super("Spawn Pig",game);
		cost = 50;
		coolDown = 1000;
	}
	
	public void onCast(EntityLogical caster) {
		super.onCast(caster);
		
	}
	public void effect() {
		game.addEntity(new EntityPig(game,game.UID));
	}
}


