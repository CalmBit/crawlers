package main;

import java.io.IOError;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import paulscode.sound.SoundSystem;
/*import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;*/


import static org.lwjgl.opengl.GL11.*;

public class TheGameMain {
	Scanner scanner = new Scanner(System.in);
	String name;
	Game game = new Game(name);
	RenderHandler gameren = new RenderHandler(game);
	public EntityPlayer player = new EntityPlayer(game);
	
	float textureWidth;
	float textureHeight;
	float width;
	float height;
	int splashTick = 240;
	
	
	
	public int state = 2;
	



	
	
 public void start() throws IOException {
	try {
		Display.setDisplayMode(new DisplayMode(game.screenX,game.screenY));
		Display.create();
	} 
	catch(LWJGLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	
	init();
	game.init();
	game.lastFPS = game.getTime();
	
	while(!Display.isCloseRequested()) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		switch(game.state) {
		case 0: //main menu
			glClearColor(0f,0f,0f,1);
			this.game.menuUpdate();
			//Color.white.bind();
			//renderMenu();
			break;
		case 1: //game
			if(game.isDebugMode) {
			game.font.drawString(game.screenX-100, 30, "DEBUG");
			player.inventory.pIng[1] = new IngredientStack(IngredientDeclerationContainer.greendust, 99.7f);
			player.inventory.pIng[2] = new IngredientStack(IngredientDeclerationContainer.gunpowder, 99.2f);
			}
			glClearColor(0f,0f,0f,0f);
			game.font.drawString(0, 0, "Crawlers " + game.version);
			game.tick();
			gameren.render();
			game.player = player;
			game.player.x = player.x;
			game.player.y = player.y;
			break;
		case 2: //splash
			if(splashTick >= 1) {
			game.splashRender();
			}
			else game.state = 0;
			splashTick--;
			break;
		case 3: //load game
			game.loadGameScreen();
			break;
		case 4: //settings
			game.settingsScreen();
			break;
		case 5: //new game
			game.newGameScreen();
		default:
			break;
		}
		
		updateFPS();
		Display.update();
		Display.sync(120);
		game.constantTicks();
		 if (this.game.isQuitRequested)
	      {
	        break;
	      }
	}
	game.cleanUp();
	Display.destroy();
 }

 
 public void init() {
	 
	
	 glMatrixMode(GL_PROJECTION);
	 glLoadIdentity();
	 glOrtho(0,game.screenX,game.screenY,0,1,-1);
	 glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D); 
	 glEnable(GL_BLEND);
	 glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	 //game.tick();
	 
	  
	 
 }
 
 public void textureLoad() throws IOException {
	//menuTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/menu.png"));
 }
 public void renderMenu() throws IOException {
	 /*menutexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/menu.png"));
	  textureWidth  = menutexture.getWidth();
	 textureHeight = menutexture.getHeight();
	 width = menutexture.getImageWidth();
	 height = menutexture.getImageHeight();
	 menutexture.bind();
		
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(13f, 450);
		glTexCoord2f(0+textureWidth, 0);
		glVertex2f(13f+width, 450);
		glTexCoord2f(0+textureWidth, 0+textureHeight);
		glVertex2f(13f+width, 450+height);
		glTexCoord2f(0, 0+textureHeight);
		glVertex2f(13f, 450+height);
		glEnd();*/
	 
 }
 public void updateFPS() {
		if (game.getTime() - game.lastFPS > 1000) {
	        game.fpsDisp = game.fps;
	        game.fps = 0; //reset the FPS counter
	        game.lastFPS += 1000; //add one second
	    }
	    game.fps++;
	}
 

 
 public static void main(String[] args) throws IOException {
	 TheGameMain thegame =  new TheGameMain();
	 thegame.start();
 }
 }

//if you are reading this, you are more than likley a cracker.
//congrats, you get to sit through some of the ugliest code made
//by humans. I support modification of my work for non-profit personal
//use. As soon as distribution becomes for profit without permission, i draw the line.
//feel free to crack this code, and if you end up using this
//in a project, just tell me and ill sponsor it
//or some crap like that. Just send me the word,
//EB5473@gmail.com. Not that hard.
//-Thanks,
//Ethan Brooks, (C) 12/27/12
