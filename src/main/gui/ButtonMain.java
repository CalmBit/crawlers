package main.gui;

import java.io.PrintStream;

import main.Game;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

public abstract class ButtonMain
{
  public String buttontext;
  public float x;
  public float y;
  public float sx = 512;
  public float sy = 128;
  public boolean vis;
  public float r;
  public float g;
  public float b = 0.5F;
  public Game game;
  public Texture texture;
  
  public ButtonMain(String text, float x, float y, float sx, float sy, boolean vis, Game game, Texture texture)
  {
    this.buttontext = text;
    this.x = x;
    this.y = y;
    this.sx = sx;
    this.sy = sy;
    this.vis = vis;
    this.game = game;
    this.texture = texture;
  }

  public void render()
  {
    if (this.vis) {
      texture.bind();
      GL11.glBegin(GL11.GL_QUADS);
      GL11.glColor3f(this.r, this.g, this.b);
      GL11.glTexCoord2f(0, 0);
      GL11.glVertex2f(x,y);
      GL11.glTexCoord2f(1, 0);
      GL11.glVertex2f(x+sx, y);
      GL11.glTexCoord2f(1, 1);
      GL11.glVertex2f(x + sx, y + sy);
      GL11.glTexCoord2f(0, 1);
      GL11.glVertex2f(x,y + sy);
      GL11.glEnd();
    }
  }

  public abstract void onClick();

  public void update()
  {
    if (game.mouseX() <= this.x + this.sx && game.mouseX() >= this.x && game.mouseY() <= this.y + this.sy && game.mouseY() >= this.y)
    {
      if (Mouse.isButtonDown(0))
      {
        onClick();
        if(game.isDebugMode) {
        System.out.println("[DEBUG] Button " + this.buttontext + " should have executed.");
        }
      }
      else
      {
        this.r += 0.1F;
        this.g += 0.1F;
        this.b += 0.1F;
      }
    }
    else if ((this.r != 0.5F) || (this.g != 0.5F) || (this.b != 0.5F))
    {
      this.r = 0.5F;
      this.g = 0.5F;
      this.b = 0.5F;
    }
  }
}