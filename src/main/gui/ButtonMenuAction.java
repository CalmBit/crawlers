package main.gui;

import java.io.PrintStream;

import org.newdawn.slick.opengl.Texture;

import main.Game;

public class ButtonMenuAction extends ButtonMain
{
  public int turn;
  

  public ButtonMenuAction(String text, float x, float y, float sx, float sy, boolean vis, int turn, Game game, Texture texture)
  {
    super(text, x, y, sx, sy, vis, game, texture);
    this.turn = turn;
  }

  public void onClick()
  {
    if (this.game.state != this.turn && game.buttonTimer <= 1) {
      this.game.state = this.turn;
      game.buttonTimer = 30;
    }
  }
}