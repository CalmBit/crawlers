package main.gui;

import org.newdawn.slick.opengl.Texture;

import main.Game;

public class ButtonReturnBoolean extends ButtonMain
{
  public boolean returnB;

  public ButtonReturnBoolean(String text, float x, float y, float sx, float sy, boolean vis, Game game, Texture texture)
  {
    super(text, x, y, sx, sy, vis, game, texture);
  }

  public void onClick()
  {
    this.returnB = true;
  }
}