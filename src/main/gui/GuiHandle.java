package main.gui;

import org.lwjgl.input.Mouse;

import main.Game;
import static org.lwjgl.opengl.GL11.*;

public class GuiHandle {
	Game game;
	public boolean guiEnabled = true;
	public boolean disableProcesses;
	public float r,g,b;
	public GuiHandle(Game game) {
		this.game = game;
		this.r = .5f;
		this.g = .5f;
		this.b = .5f;
	}
	
	public void render() {
		
	}
	
	public void handle() {
		
	}
	
	public void doNothing() {
		
	}
	
}
