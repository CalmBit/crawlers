package main.gui;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;


import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.XRandR.Screen;
import org.newdawn.slick.Color;

import main.EntityPlayer;
import main.Game;
import main.ItemStack;
import main.PlayerInventory;


public class PlayerGUI extends GuiHandle {
	public Game game;
	public PlayerInventory inventory;
	public boolean craftDisplay;
	public float manadim;
	public PlayerGUI(Game game, PlayerInventory inventory) {
		super(game);
		this.game = game;
		this.inventory = inventory;
	}
	public void render() { 
		manadim = (float)game.pm / 128;
		Game.healthBackTexture.bind();
		glBegin(GL_QUADS);
		//render red back
		glColor3f(1f,0f,1f);
		glTexCoord2f(0,1);
		glVertex2f(0,game.screenY);
		glTexCoord2f(1,1);
		glVertex2f(0 + 128, game.screenY);
		glTexCoord2f(1,0);
		glVertex2f(0 + 128, game.screenY - 32);
		glTexCoord2f(0,0);
		glVertex2f(0 , game.screenY - 32);
		glEnd();
		//render green front, variable
		
		Game.healthTexture.bind();
		glBegin(GL_QUADS);
		glColor3f(0f,1f,0f);
		glTexCoord2f(0,1);
		glVertex2f(0,game.screenY);
		glTexCoord2f((game.ph / 64),1);
		glVertex2f(0 + game.ph * 2, game.screenY);
		glTexCoord2f((game.ph / 64),0);
		glVertex2f(0 + game.ph * 2, game.screenY - 32);
		glTexCoord2f(0,0);
		glVertex2f(0 , game.screenY - 32);
		glEnd();
		//render mana blue
		Game.manaTexture.bind();
		glBegin(GL_QUADS);
		glColor3f(0f,0f,1f);
		glTexCoord2f(0,1);
		glVertex2f(0,game.screenY-32);
		glTexCoord2f(manadim,1);
		glVertex2f(0 + game.pm, game.screenY-32);
		glTexCoord2f(manadim,0);
		glVertex2f(0 + game.pm, game.screenY - 64);
		glTexCoord2f(0,0);
		glVertex2f(0 , game.screenY - 64);
		glEnd();

		glBegin(GL_QUADS);
		//render cooldown red
		glColor3f(1f, 0f, 0f);
		glVertex2f(game.screenX - game.cc/10, 0);
		glVertex2f(game.screenX, 0);
		glVertex2f(game.screenX, 25);
		glVertex2f(game.screenX - game.cc/10, 25);
		
		//render item use green
		glColor3f(0f,1f,0f);
		glVertex2f(game.screenX - game.ut/5, 25);
		glVertex2f(game.screenX, 25);
		glVertex2f(game.screenX, 50);
		glVertex2f(game.screenX - game.ut/5, 50);
		
		
		/*//render pinv
		glColor3f(0, 1, 1);
		glVertex2f(150, 10);
		glColor3f(1, 0, 0);
		glVertex2f(150, 60);
		glColor3f(0, 1, 0);
		glVertex2f(950, 60);
		glColor3f(0, 0, 1);
		glVertex2f(950, 10);*/
		
	    //render durability
		if(inventory.pInv[inventory.selection] instanceof ItemStack) {
		glColor3f(0,0,1);
		glVertex2f(game.screenX,game.screenY);
		glVertex2f(game.screenX - inventory.pInv[inventory.selection].item.damage,game.screenY);
		glVertex2f(game.screenX - inventory.pInv[inventory.selection].item.damage,game.screenY - 25);
		glVertex2f(game.screenX,game.screenY-25); }
		
		if(craftDisplay)
		{
			renderCraftingGUI();
			disableProcesses = true;
		}
		else
		{
			disableProcesses = false;
		}
		glEnd();
		
		
	}
	
	public void renderCraftingGUI()
	{
		glColor3f(.5f,.5f,.5f);
		//lefthand bar
		glVertex2f(0,game.screenY-350);
		glVertex2f(0,game.screenY);
		glVertex2f(25,game.screenY);
		glVertex2f(25,game.screenY-350);
		//righthand bar
		glVertex2f(675,game.screenY-350);
		glVertex2f(675,game.screenY);
		glVertex2f(700,game.screenY);
		glVertex2f(700,game.screenY-350);
		//top bar
		glVertex2f(25,game.screenY);
		glVertex2f(25,game.screenY-25);
		glVertex2f(675,game.screenY-25);
		glVertex2f(675,game.screenY);
		//bottom bar
		glVertex2f(25,game.screenY-325);
		glVertex2f(25,game.screenY-350);
		glVertex2f(675,game.screenY-350);
		glVertex2f(675,game.screenY-325);
		
		glVertex2f(25,game.screenY-25);
		glVertex2f(25,game.screenY-325);
		glVertex2f(675,game.screenY-325);
		glVertex2f(675,game.screenY-25);
		//crafting interface will have multi slots, full square for now
		/*//first piece
		glVertex2f(25,game.screenY-25);
		glVertex2f(25,game.screenY-108);
		glVertex2f(675,game.screenY-108);
		glVertex2f(675,game.screenY-25);
		//second alternating strip
		glVertex2f(25,game.screenY-108);
		glVertex2f(25,game.screenY-216);
		glVertex2f(202,game.screenY-216);
		glVertex2f(202,game.screenY-108);*/
		
		
		
	}
}


