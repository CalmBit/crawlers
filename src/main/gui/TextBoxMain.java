package main.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import main.Game;
import static org.lwjgl.opengl.GL11.*;

public class TextBoxMain extends InterfaceSelectable {
	
	public int x,y,sx,sy;
	public Game game;
	public String contents;
	public boolean hooked;
	public float r,g,b;
	public TextBoxMain(int x, int y, int sx, int sy, Game game)
	{
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.game = game;
		contents = "";
		hooked = false;
		r = .6f;
		g = .6f;
		b = .6f;
		
	}
	public void hookIntoTextBox()
	{
		if(!hooked) {
		game.currentlySelected = this;
		hooked = true;
		r = 1;
		b = 0;
		g = 0;
		}
	}
	
	public void update()
	{
		while(Keyboard.next() && hooked)
		{
			char add = Keyboard.getEventCharacter();
			if(Keyboard.getEventKey() == Keyboard.KEY_BACK && Keyboard.getEventKeyState() && contents != "")
			{
				if(contents.length() >= 1)
				{
					if(contents.length() != 1) {
					contents = contents.substring(0, contents.length()-1);
					continue;
					}
					else contents = "";
				}
			}
			if(Keyboard.getEventKeyState() && !((Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) && !(Character.isAlphabetic(add)||Character.isDigit(add))&& !Keyboard.isKeyDown(Keyboard.KEY_BACK)))
			{
			contents += add;
			}
			
		}
		if(game.mouseX() <= x+sx && game.mouseX() >= x && game.mouseY() >= y && game.mouseY() <= y+sy && Mouse.isButtonDown(0))
		{
			hookIntoTextBox();
		}
		else if(Mouse.isButtonDown(0) && hooked)
		{
			exitFromTextBox();
		}
	}
	public void render()
	{
		glColor3f(r,g,b);
		glBegin(GL_LINES);
		glVertex2f(x,y);
		glVertex2f(x+sx,y);
		
		glVertex2f(x+sx,y);
		glVertex2f(x+sx,y+sy);
		
		glVertex2f(x+sx,y+sy);
		glVertex2f(x,y+sy);
		
		glVertex2f(x,y+sy);
		glVertex2f(x,y);
		glEnd();
		if(contents.length() <= 21) {
		game.font.drawString(x, y, contents);
		}
		else
		{
		game.font.drawString(x, y, contents.subSequence(contents.length() - 22, contents.length()-1));
		}
	}
	public void exitFromTextBox()
	{
		game.currentlySelected = null;
		hooked = false;
		r = .6f;
		g = .6f;
		b = .6f;
		game.name = contents;
	}
}
